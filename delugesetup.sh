#!/bin/bash
#By Network Silence

# The following script will Install and configure a Webui Deluge client with the default port being <IP>:8112
# The Default password is : "deluge"

#USAGE:
#     Mountpath visible in systemctl -t mount
#          ex: torrentmount.mount
#
#     delugesetup.sh <FullMount Path>
#
#  If left blank this will assume you dont want to be reliant on a drive to be mounted and will proceed anyways


if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi


if [[ $# -eq 1 ]] && [[ $1 = *.mount ]]
then
  	mountName=$1
elif [[ $# -ne 1 ]]
then
	echo "No Mount Reliance"
	mountName=""
else
	echo "Mount point not valid please include complete path including .mount"
	systemctl -t mount
	exit 1
fi


delugeService="/etc/systemd/system/deluged.service"
delugeWebService="/etc/systemd/system/deluge-web.service"


#----------------------------------Setup User
apt-get update -y
apt-get install deluged -y && apt-get install deluge-web -y
adduser --system  --gecos "Deluge Service" --disabled-password --group --home /var/lib/deluge deluge
adduser $USER deluge

#---------------------------------Setup Configs for SystemD
#View mounts with `systemctl -t mount`
tee $delugeService <<EOF > /dev/null
[Unit]
Description=Deluge Bittorrent Client Daemon
Documentation=man:deluged
After=network-online.target $mountName
Requires=$mountName
BindsTo=$mountName
[Service]
Type=simple
User=deluge
Group=deluge
UMask=007
ExecStart=/usr/bin/deluged -d -l /var/log/deluge/daemon.log -L warning error critical
Restart=on-failure
# Time to wait before forcefully stopped.
TimeoutStopSec=300
[Install]
WantedBy=multi-user.target $mountName
EOF


tee $delugeWebService <<EOF > /dev/null
[Unit]
Description=Deluge Bittorrent Client Web Interface
Documentation=man:deluge-web
After=network-online.target deluged.service
Wants=deluged.service
[Service]
Type=simple
User=deluge
Group=deluge
UMask=027
# This 5 second delay is necessary on some systems
# to ensure deluged has been fully started
ExecStartPre=/bin/sleep 5
ExecStart=/usr/bin/deluge-web -l /var/log/deluge/web.log -L warning error critical
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOF

#--------------------------------------Run SystemD service
systemctl enable /etc/systemd/system/deluged.service
systemctl enable /etc/systemd/system/deluge-web.service
mkdir -p /var/log/deluge
chown -R deluge:deluge /var/log/deluge
chmod -R 750 /var/log/deluge
systemctl start deluged
systemctl start deluge-web

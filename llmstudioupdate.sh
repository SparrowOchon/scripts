#!/bin/bash

if [[ "$#" -ne 1 ]];then
   echo "Usage: $0 <version>"
   exit 1
fi

version=$1
temp_dir=$(mktemp -d)  # Create a temporary directory for downloading the AppImage file

url="https://releases.lmstudio.ai/linux/x86/$version/beta/LM_Studio-$version.AppImage"

wget "$url" -O "$temp_dir/LM_Studio.AppImage" || { echo "Failed to download the AppImage file."; exit 1; }
chmod +x "$temp_dir/LM_Studio.AppImage"

sudo mv "$temp_dir/LM_Studio.AppImage" /usr/local/bin/llmstudio || { echo "Failed to replace the existing llmstudio binary."; exit 1; }
rm -r "$temp_dir"  # Remove the temporary directory

if [ $? -eq 0 ]; then
   echo "Successfully replaced the existing llmstudio binary with the new version $version."
else
   echo "Failed to replace the existing llmstudio binary with the new version $version."
fi
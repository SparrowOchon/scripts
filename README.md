# List of Scripts

### Machine AutoRestart on VPN fail

- checkvpn.sh <- Service
- runner.sh <- Runner launched by service

### Godot AutoUpdater from Latest Source

- godotupdate.py

### Setup Deluge on headless server with WEBUI

- delugesetup.sh

### Git CLI setup

- gitcli.sh

### USB port and BUS info

- portinfo.sh

### Rebuild DKMS modules for installed kernels

- dkmsrebuild.sh

### Fetch name of process closest to the entered process name based on psutil

- processnames.py

### Set default branch to testing and add callable sid packages

- debunstable.sh

### Disable windows access using /etc/cron.daily for all points till 9pm

- disable_windows

### Dmi info dump for error reporting

- sysinfodump.sh

### Run python codebase validation tools

- pyvalidate (Linux Version)
- pyvalidate (Windows Version)
- run_pyvalidate.bat (Windows to run as external tool on Pycharm)

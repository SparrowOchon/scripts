#!/bin/bash
#By: Network Silence
# The following lets you download from both your standard branch and
# Unstable without having to worry to much about upgrades
#
# Usage: apt-get -t unstable install <package>
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

releaseDir="/etc/apt/apt.conf.d/99defaultrelease"
sourcesList="/etc/apt/sources.list"

tee $releaseDir <<EOF > /dev/null
APT::Default-Release "testing";
EOF

tee -a $sourcesList <<-EOF > /dev/null
#The following are only used with apt-get -t unstable otherwise testing is used
deb     http://ftp.de.debian.org/debian/    unstable main contrib non-free
deb-src http://ftp.de.debian.org/debian/    unstable main contrib non-free
EOF

apt-get update

#!/bin/bash
#   By Network Silence

#
#   The following is run by the checkvpn.sh init process
#
#

while :
do
    curProcesses=$(ps -aux | grep "openvpn" | wc -l)
    if [ $curProcesses -lt 4 ];then
        shutdown -r now
    fi
    sleep 1s
done
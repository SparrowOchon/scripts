"""
FILENAME :        processnames

DESCRIPTION :
        Find Processes named similarly to the user inputed parameter.
        This is Case insensitive I.e Everything is lowercased
        Usage:
            python processnames.py <PROCESS NAME>


NOTES :
       Only compares against active processes.
       Python equivalent to ps -eo comm=, | grep -i <PROCESS NAME>

AUTHOR :   Network Silence        START DATE :    07 December 2018


License;

BSD 2-Clause License

Copyright (c) 2018, Network Silence
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import os
import psutil
import sys

if len(sys.argv) != 2:
    print('Usage: python processnames.py <PROCESS NAME>')
    print('Replace <PROCESS NAME> with process you are looking for. Case Insensitive')
else:
    userProcess = sys.argv[1]
    print('Listing similar named running processes found by psutil:')
    for proc in psutil.process_iter():
        try:
            (path, exe) = os.path.split(proc.exe())
        except (psutil.Error, OSError):
            pass
        else:
            if userProcess.lower() in exe.lower():
                print(exe)

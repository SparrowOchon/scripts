#!/bin/bash
#By:Network Silence
# This is just the simple Git Cli setup commands in one place
# To avoid looking for the basics every time for new machines
#
if [ $# -ne 2 ]; then
    echo $0: usage: ./gitcli.sh \"Username\" \"GIT REPOS PATH\"
    exit 1
fi
if [ ! -d "$2" ] ; then
    echo "$2 is not a directory";
    exit 1
fi
username="$1"
path="$2"
git config --global user.name $username
git config --global --add includeIf."gitdir:/media/null/Store/Repos/Gitlab/" path "/media/null/Store/Repos/Gitlab/.gitconfig"
git config --global --add includeIf."gitdir:/media/null/Store/Repos/Github/" path "/media/null/Store/Repos/Github/.gitconfig"
git config --global color.ui auto
git config --global color.branch auto
git config --global color.status auto
git config --global core.editor "vim"
git config --global merge.tool meld
git config --global diff.tool meld
git config --global mergetool.meld.trustExitCode false
git config --global mergetool.meld.cmd "meld \"\$LOCAL\" \"\$MERGED\" \"\$REMOTE\" --output \"\$MERGED\""
git config --global difftool.meld.cmd "meld \"\$LOCAL\" \"\$REMOTE\""
git config --global merge.conflictstyle diff3
git config --global mergetool.prompt false
git config --global difftool.prompt false
git config --global url.git@github.com:.insteadOf https://github.com/
git config --global url.git@gitlab.com:.insteadOf https://gitlab.com/
echo
echo
echo "Setup Git emails"
mkdir -p $path/Github
mkdir -p $path/Gitlab
cat > $path/Github <<- "EOF"
[user]
    email = 42444069+SparrowOchon@users.noreply.github.com
EOF
cat > $path/Gitlab <<- "EOF"
[user]
    email = 2419495-SparrowOchon@users.noreply.gitlab.com
EOF

echo
echo "Gitlab SSH key"
ssh-keygen -t rsa -C "$email" -f ~/.ssh/id_rsa_gitlab
ssh-add ~/.ssh/id_rsa_gitlab
echo "Github SSH Key"
ssh-keygen -t rsa -C "$email" -f ~/.ssh/id_rsa_github
ssh-add ~/.ssh/id_rsa_github
echo "Making SSH config"
cat > ~/.ssh/config <<- "EOF"
# Personal github account
Host github.com
    HostName github.com
    User $username
    IdentityFile ~/.ssh/id_rsa_github
# Personal gitlab account
Host gitlab.com
    HostName gitlab.com
    User $username
    IdentityFile ~/.ssh/id_rsa_gitlab
EOF

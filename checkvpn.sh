#!/bin/bash
# By: Network Silence
# The following is a init.d script that checks and restarts
# the vpn connection when it detects that it is not running
#
# To avoid Leaking while trying to reconnect add the following
# line to crontab "@reboot /usr/sbin/openvpn /path/to/openvpn.conf &"
#


# Usage : Requires Root Privilages
# vim /etc/rc.local
# add script fullpath before the exit 0
#
# OR
#
# Add to /etc/init.d/<name>.sh
# and run "update-rc.d <name>.sh defaults"


start() {
  #Code that runs on start
  /etc/openvpn/runner.sh &
 echo "$!" > /etc/openvpn/runner_pid.txt
}

stop() {
  #Code that runs on stop
  runnerPid=`cat /etc/openvpn/runner_pid.txt`
  kill -9 $runnerPid
}
case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  status)
    #Code to check status
    ;;
esac

﻿Param([string]$file_names)
#================================================================================================
#
# FILENAME :        pyvalidate
#
# DESCRIPTION : Automatically format and identify complexity, maintainability and security issues
#		within a python script.
#
# USAGE:
#		powershell .\pyvalidate.ps1 <Python Script>
#
#
# AUTHOR :   Network Silence        START DATE :    17 May 2021
#
#
# LICENSE :
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
#	Copyright (c) 2019, Network Silence
#		All rights reserved.
#
#
#
#
#===============================================================================================
function print_error($error_msg){
	Write-Host "USAGE: $MyInvocation.MyCommand.Name <File to Analyze>"
	Write-Host "$error_msg"
	exit
}
function argument_validate(){
	if ([string]::IsNullOrEmpty($file_names)) {
		print_error "Parameters missing."
	}
}
function validate_file(){
    Foreach($file_name in $file_names){
		If ( -Not (Test-Path -Path $file_name -PathType Leaf)) {
			print_error "File $file_name does not exist"
		}
		black --quiet "$file_name"
		Write-Host "====================================Code Errors============================================================="
		pylint --disable=E0401,E1101 "$file_name"
		Write-Host "====================================Vulnerability Search===================================================="
		bandit -ii "$file_name"
		Write-Host "====================================Complexity Identify====================================================="
		Write-Host "A	low - simple block"
		Write-Host "B	low - well structured and stable block"
		Write-Host "C	moderate - slightly complex block"
		Write-Host "D	more than moderate - more complex block"
		Write-Host "E	high - complex block, alarming"
		Write-Host "F	very high - error-prone, unstable block"
		Write-Host
		radon cc --max C "$file_name"
		Write-Host "====================================Maintainability Score==================================================="
		Write-Host "100 - 20	A	Very high"
		Write-Host "19 - 10		B	Medium"
		Write-Host "9 - 0		C	Extremely low"
		Write-Host
		radon mi -s "$file_name"
	}
}
function main(){
#	Write-Host "Updating Packages"
#	pip3 install black pylint bandit radon -U >/dev/null
    
	argument_validate
	validate_file
}
main
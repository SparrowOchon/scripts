#!/bin/bash
#By: Ryan Thompson
# https://askubuntu.com/questions/53364/command-to-rebuild-all-dkms-modules-for-all-installed-kernels
# Rebuild all DKMS modules for all installed kernels
ls  /var/lib/initramfs-tools | sudo xargs -n1 /usr/lib/dkms/dkms_autoinstaller start
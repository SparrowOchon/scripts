#!/usr/bin/python3
import subprocess
import os
import requests
import re

REGEXSTRING = re.compile("^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)(?:\.(?P<patch>0|[1-9]\d*)*)?$", re.MULTILINE)

def get_branches(username, repository):
    url = f"https://api.github.com/repos/{username}/{repository}/branches"
    headers = {"Accept": "application/vnd.github+v3"}
    response = requests.get(url, headers=headers)
    branches = response.json()
    return branches


def extract_major_minor_patch(version):
    version = version.replace("'", "")
    parts = re.search(REGEXSTRING, version)
    if parts is None:
        return [0,0,0]
    groups = parts.groups()
    major, minor, patch = map(lambda x: int(x or 0), groups)
    return [major, minor, patch]


def find_largest_version(branch_list):
    largest_version = [0, 0, 0]
    largest_index = 0
    for index, branch in enumerate(branch_list):
        version_number = extract_major_minor_patch(branch['name'])
        if (version_number[0] > largest_version[0]) or (version_number[0] == largest_version[0] and version_number[1] > largest_version[1]) or (version_number[0] == largest_version[0] and version_number[1] == largest_version[1] and version_number[2] > largest_version[2]):
            largest_version = version_number
            largest_index = index
    return largest_index

def execute_godot_build(branch):
    os.chdir("/usr/local/src/godot")

    # Run git pull and git fetch commands
    subprocess.run(["git", "pull"], check=True)
    subprocess.run(["git", "fetch"], check=True)
    # Checkout to specified branch
    subprocess.run(["git", "checkout", branch], check=True)
    try:
        subprocess.run(["rm", "/usr/local/src/godot/bin/*"], check=True)
    except subprocess.CalledProcessError as e:
        print("No godot binary found in bin directory:", e)
    # Run scons command with provided arguments
    cpu_count = os.cpu_count()
    command = [
        "scons",
        f"-j{cpu_count}",
        "platform=linuxbsd",
        "target=editor",
        "linker=mold",
        "production=yes",
        "arch=x86_64",
        "lto=full"
    ]
    subprocess.run(command, check=True)

username = "godotengine"
repository = "godot"
branch_list = get_branches(username, repository)
index = find_largest_version(branch_list)
execute_godot_build(branch_list[index]['name'])
#!/bin/bash
# By: Network Silence
#
#Get information on devices connected through USB
#
#
for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
    (
        syspath="${sysdevpath%/dev}"
        devname="$(udevadm info -q name -p $syspath)"
        [[ "$devname" == "bus/"* ]] && continue
        eval "$(udevadm info -q property --export -p $syspath)"
        [[ -z "$ID_SERIAL" ]] && continue
        echo "/dev/$devname - $ID_SERIAL"
    )
done
# cat /sys/kernel/debug/usb/devices
echo
echo " USBBUS and Device Name (usbmon<bus>) "
echo 
lsusb | grep -v 'Linux'